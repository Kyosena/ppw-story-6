from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse
from .views import index,redirect
from .models import Statuses
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

class Lab6UnitTest(TestCase):
    #URLS and VIEWS
    def test_url_isexist(self):
        response = Client().get(reverse('status'))
        self.assertEquals(response.status_code,200)
    def test_sign_url_resolve(self):
        url = reverse('status')
        self.assertEquals(resolve(url).func,index) 
    def test_redirect_url_resolve(self):
        url=reverse('redirect')
        response = Client().get(url)
        self.assertEquals(response.status_code,302)
    #FORMS
    def test_forms_input_html(self):
        form = StatusForm()
        self.assertIn('id="id_status"', form.as_p())
    #MODELS
    def test_model_createstatus(self):
        newstat = Statuses.objects.create(status = " Burned out ")
        counter = Statuses.objects.all().count()
        self.assertEquals(counter,1)
    def create_blank_status(self):
        return Statuses.objects.create()
    def create_status(self,status = "this is a test son"):
        return Statuses.objects.create(status = status)
    def test_creation(self):
        s = self.create_status()
        b = self.create_blank_status()
        self.assertTrue(isinstance(s,Statuses))
        self.assertTrue(isinstance(b,Statuses))
        self.assertEqual("this is a test son",s.status)
        self.assertEqual("I'm good",b.status)




# class Lab6FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         self.selenium  = webdriver.Chrome()
#         super(Lab6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab6FunctionalTest, self).tearDown()

#     def test_input_status(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://localhost:8000/PPW6TDD/status/')
#         # find the form element
#         status = selenium.find_element_by_id('id_status')
#         submit = selenium.find_element_by_id('btn')

#         # Fill the form with data
#         status.send_keys('Coba Coba')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)

#         time.sleep(3)
#         self.assertIn("Coba Coba", selenium.page_source)


# Create your tests here.
