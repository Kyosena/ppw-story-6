from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from .forms import StatusForm
from .models import Statuses

# Create your views here.
def index(request):
    form = StatusForm()
    if request.method == 'POST':
        print('test')
        data = Statuses(status = request.POST['status'])
        data.save()
    arguement = {
        'StatusList' : Statuses.objects.all(),
        'Statusform' : StatusForm
    }
    return render(request,"index.html",arguement)
def redirect(request):
    return HttpResponseRedirect('PPW6TDD/status/')